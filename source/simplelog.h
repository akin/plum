#ifndef EXAMPLE_SIMPLELOG_HEADER
#define EXAMPLE_SIMPLELOG_HEADER

#include <tunnel/ilog.h>
#include <string>
#include <iostream>
#include <fstream>

class SimpleLog : public tunnel::ILog
{
private:
	std::ofstream m_output;
protected:
	virtual void write(tunnel::LogLevel level, size_t count, void *buf) override;
public:
	virtual ~SimpleLog();

	void setup(std::string filename);
};

#endif