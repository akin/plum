
#include <iostream>
#include <tunnel/config.h>

#include <tunnel/platform.h>
#include <tunnel/helpers/parservar.h>
#include <tunnel/var.h>
#include "simplelog.h"

int main(int argc, char **argv)
{
	SimpleLog log;
	log.setup("log.txt");
	
	LOG_SET(log);
	tunnel::Parser::args(argc, argv, VARS);

	std::cout << "hello" << std::endl;
	return 0;
}
