#include "simplelog.h"
#include <vector>
#include <cstring>
#include <iostream>

SimpleLog::~SimpleLog()
{
	m_output.close();
}

void SimpleLog::write(tunnel::LogLevel level, size_t count, void *buf)
{
	std::string str(static_cast<char*>(buf));
	
	std::cout << str << std::endl;

	if (m_output.is_open())
	{
		m_output << str << std::endl;
	}
}

void SimpleLog::setup(std::string filename)
{
	m_output.open(filename);
}
