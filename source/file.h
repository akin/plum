#ifndef PROJECT_FILE_HEADER
#define PROJECT_FILE_HEADER

#include <tunnel/ifile.h>
#include <fstream>

class File : tunnel::IFile
{
private:
	std::ifstream m_stream;
	size_t m_filesize = 0;
	size_t m_position = 0;
public:
	File();
	bool open(std::string path);
	virtual ~File();
	
	virtual bool canRead() const override;
	
	virtual bool read(size_t count, void *buf) override;
	
	virtual size_t setPosition(size_t at) override;
	virtual size_t getPosition() const override;

	virtual bool isGood() const override;

	virtual size_t size() const override;
	virtual size_t remaining() const override;
};

#endif