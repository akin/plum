#include "file.h"
#include <sys/stat.h>
#include <iostream>

File::File()
{
}

File::~File()
{
}

bool File::open(std::string path)
{
	struct stat results;
	if (stat(path.c_str(), &results) == 0)
	{
		m_filesize = results.st_size;
	}
	else
	{
		return false;
	}
	m_stream.open(path, std::ios::in | std::ios::binary);
	m_position = 0;
	return canRead();
}

bool File::canRead() const
{
	return m_stream.is_open() && (!m_stream.fail());
}

bool File::read(size_t count, void *buf)
{
	if (!m_stream.read(static_cast<char*>(buf), count))
	{
		return false;
	}
	m_position += count;
	return true;
}

size_t File::setPosition(size_t at)
{
	m_position = at;
	m_stream.seekg(at);
	if (m_stream.bad())
	{
		m_position = m_stream.tellg();
		return false;
	}
	return true;
}

size_t File::getPosition() const
{
	// JESUS! tellg is not const! WTF
	//return static_cast<size_t>(m_stream.tellg());
	return m_position;
}

bool File::isGood() const
{
	return !m_stream.fail();
}

size_t File::size() const
{
	return m_filesize;
}

size_t File::remaining() const
{
	return m_filesize - m_position;
}
