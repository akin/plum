call git clone https://github.com/actor-framework/actor-framework.git caf
call git clone https://github.com/g-truc/glm.git glm
call git clone https://github.com/assimp/assimp.git assimp
call git clone https://github.com/nothings/stb.git stb
REM call git clone https://github.com/nlohmann/json.git json
call git clone https://github.com/dougbinks/enkiTS.git enkiTS
call git clone https://github.com/jonasmr/microprofile.git microprofile
call git clone https://github.com/dreamworksanimation/openvdb.git openvdb
