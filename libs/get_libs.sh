git clone https://github.com/actor-framework/actor-framework.git caf
git clone https://github.com/g-truc/glm.git glm
git clone https://github.com/assimp/assimp.git assimp
git clone https://github.com/nothings/stb.git stb
git clone https://github.com/nlohmann/json.git json
git clone https://github.com/dougbinks/enkiTS.git enkiTS
git clone https://github.com/jonasmr/microprofile.git microprofile
git clone https://github.com/dreamworksanimation/openvdb.git openvdb
